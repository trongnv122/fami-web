import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subject, takeUntil} from "rxjs";
import {UserService} from "../../service/user.service";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {ExamService} from "../../service/exam.service";
import {ListRequest} from "../../models/list.request";
import {Exam} from "../../models/exam";

@Component({
  selector: 'app-login',
  templateUrl: './content-layout.component.html',
  styleUrls: ['./content-layout.component.scss']
})

export class ContentLayoutComponent implements OnInit, OnDestroy, AfterViewInit {
  public title = 'Tiểu học';
  public isAuthenticated = false;
  private _destroySub$ = new Subject<void>();
  displayedColumns: string[] = ['startDate', 'name', 'point', 'takeTime'];
  @ViewChild(MatPaginator) paginator: MatPaginator | null = null;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  data: Exam[] = [];
  dataSource = new MatTableDataSource<Exam>(this.data);
  TotalRecord: number = 0;
  PageSize: number = 0;
  PageNum: number = 0;

  constructor(
    private userService: UserService,
    private examService: ExamService
  ) {
  }

  public ngOnInit(): void {
    this.userService.userSubject.pipe(
      takeUntil(this._destroySub$)
    ).subscribe(user => {
      this.isAuthenticated = user != null;
    });

    this.page(null);
  }

  public ngOnDestroy(): void {
    this._destroySub$.next();
  }

  public logout(): void {
    this.userService.logout();
  }

  page($event: PageEvent | null) {
    let request: ListRequest = new ListRequest();
    if ($event == null) {
      this.PageSize = 25;
      this.PageNum = 0;
    }
    request.size = this.PageSize;
    request.page = this.PageNum;

    this.examService.list(request).subscribe(res => {
      this.dataSource.data = res.items;
      this.TotalRecord = res.totalRecord;
    })
  }
}
