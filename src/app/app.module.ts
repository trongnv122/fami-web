import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import { AppComponent } from './app.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSelectModule} from "@angular/material/select";
import {MatIconModule} from "@angular/material/icon";
import {MatDialogModule} from "@angular/material/dialog";
import {MatCardModule} from "@angular/material/card";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {StoreModule} from "@ngrx/store";
import {FormatTimePipe} from "./format-time.pipe";
import {AppRoutes} from "./app.routing";
import {PageNotFound} from "./component/pagenotfound/page-not-found";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatMenuModule} from "@angular/material/menu";
import {ContentLayoutComponent} from "./layout/content-layout/content-layout.component";
import {LoginComponent} from "./component/login/login.component";
import {UserService} from "./service/user.service";
import {HttpClientModule} from "@angular/common/http";
import {Perimeter_areaComponent} from "./component/perimeter_area/perimeter_area.component";
import {UnitConversationComponent} from "./component/unit-conversation/unit-conversation.component";
import {ExamService} from "./service/exam.service";
import {AuthGuardService} from "./service/auth.guard.service";
import {AdditionComponent} from "./component/addition/addition.component";
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatNativeDateModule} from "@angular/material/core";
import {MatRadioModule} from "@angular/material/radio";
import {MatGridListModule} from "@angular/material/grid-list";
import {SignupComponent} from "./component/signup/signup.component";

@NgModule({
  declarations: [
    AppComponent,
    FormatTimePipe,
    PageNotFound,
    ContentLayoutComponent,
    LoginComponent,
    Perimeter_areaComponent,
    UnitConversationComponent,
    AdditionComponent,
    SignupComponent
  ],
  imports: [
    AppRoutes,
    StoreModule.forRoot({}),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    MatDialogModule,
    MatCardModule,
    MatProgressSpinnerModule,
    FormsModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatNativeDateModule,
    MatRadioModule,
    MatGridListModule
  ],
  providers: [UserService, ExamService, AuthGuardService],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
