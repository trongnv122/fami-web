import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./component/login/login.component";
import {ContentLayoutComponent} from "./layout/content-layout/content-layout.component";
import {Perimeter_areaComponent} from "./component/perimeter_area/perimeter_area.component";
import {UnitConversationComponent} from "./component/unit-conversation/unit-conversation.component";
import {AuthGuardService} from "./service/auth.guard.service";
import {AdditionComponent} from "./component/addition/addition.component";
import {SignupComponent} from "./component/signup/signup.component";

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '', pathMatch: 'full',
    component: ContentLayoutComponent
  },
  {
    path: 'perimeter_area', pathMatch: 'full',
    component: Perimeter_areaComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'unit-conversation', pathMatch: 'full',
    component: UnitConversationComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'addition', pathMatch: 'full',
    component: AdditionComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'signup', pathMatch: 'full',
    component: SignupComponent,
  }
];

export const AppRoutes = RouterModule.forRoot(routes, {
  paramsInheritanceStrategy: 'always'
});
