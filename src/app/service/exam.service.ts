import {Injectable} from "@angular/core";
import {map, Observable} from "rxjs";
import {Router} from "@angular/router";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Exam} from "../models/exam";
import {ListRequest} from "../models/list.request";
import {ListResponse} from "../models/list.response";

@Injectable()
export class ExamService {

  constructor(
    private router: Router,
    private http: HttpClient,
    private _router: Router
  ) {
  }

  submit(exam: Exam): Observable<any> {
    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'bearer ' + localStorage.getItem('token'));

    let options = {
      headers: httpHeaders
    };

    return this.http.post<any>(`${environment.apiUrl}/exam`, exam, options)
      .pipe(map(res => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        this._router.navigateByUrl('/');
      }));
  }

  list(request: ListRequest): Observable<ListResponse> {
    let httpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'bearer ' + localStorage.getItem('token'));

    let options = {
      headers: httpHeaders
    };

    return this.http.post<ListResponse>(`${environment.apiUrl}/exam/list`, request, options);
  }

}
