import {Injectable} from "@angular/core";
import {BehaviorSubject, map, Observable} from "rxjs";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {User} from "../models/user";
import {environment} from "../../environments/environment";

@Injectable()
export class UserService {

  public userSubject: BehaviorSubject<User | null>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    let userStorage = localStorage.getItem('user');
    let userStore = userStorage && userStorage !== 'undefined' ? JSON.parse(userStorage) : null;
    this.userSubject = new BehaviorSubject<User | null>(userStore);
  }

  public get userValue(): User | null {
    return this.userSubject.value;
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/user/login`, {username, password})
      .pipe(map(res => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('token', res.token);
        localStorage.setItem('user', JSON.stringify(res.user));
        this.userSubject.next(res.user);
        return res.user;
      }));
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.userSubject.next(null);
    this.router.navigate(['/login']);
  }

  authenticated(): boolean {
    let token = localStorage.getItem('token');
    return Boolean(token && token != 'undefined');
  }

  register(user: User): Observable<any> {
    return this.http.post(`${environment.apiUrl}/user/register`, user);
  }

  update(id: number, params: any) {
    return this.http.put(`${environment.apiUrl}/users/${id}`, params)
      .pipe(map(x => {
        return x;
      }));
  }

}
