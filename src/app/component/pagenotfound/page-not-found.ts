import {Component, OnInit} from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './page-not-found.html',
  styleUrls: ['./page-not-found.scss']
})
export class PageNotFound implements OnInit {


  ngOnInit(): void {
  }

}
