import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs';
import * as moment from "moment/moment";
import {UserService} from "../../service/user.service";
import {User} from "../../models/user";
import {
  AbstractControl,
  FormControl,
  FormGroup, ValidatorFn,
  Validators
} from "@angular/forms";

class CustomValidators {
  static match(controlName: string, matchControlName: string): ValidatorFn {
    return (controls: AbstractControl) => {
      const control = controls.get(controlName);
      const matchControl = controls.get(matchControlName);

      if (!matchControl?.errors && control?.value !== matchControl?.value) {
        matchControl?.setErrors({
          matching: {
            actualValue: matchControl?.value,
            requiredValue: control?.value
          }
        });
        return {matching: true};
      }
      return null;
    };
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  hide = true;

  form: FormGroup;

  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);

  }
  submitted = false;

  private _destroySub$ = new Subject<void>();

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
  ) {
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push(i);
    }
    let _moment = moment();
    let _year = _moment.year();
    let _month = _moment.month() + 1;
    let _day = parseInt(_moment.format('DD'));

    for (let i = _year - 20; i <= _year; i++) {
      this.YEARS.push(i);
    }

    this.form = new FormGroup({
        firstName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
        lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
        phone: new FormControl('', [Validators.required, Validators.pattern('^[0-9]{9,11}$')]),
        email: new FormControl('', [Validators.required, Validators.email]),
        username: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(200)]),
        password: new FormControl('', [Validators.required,Validators.minLength(6), Validators.maxLength(200)],),
        confirmPassword: new FormControl('', [Validators.required,]),
        day: new FormControl(_day),
        month: new FormControl(_month),
        year: new FormControl(_year - 6),
        class: new FormControl(1),
        gender: new FormControl(''),
      },
      {
        validators: [CustomValidators.match('password', 'confirmPassword')]
      }
    );


    this.getDays(_year, 'year');
  }

  YEARS: number[] = []
  MONTHS: number[] = []
  DAYS: number[] = []
  signupErrorMessage: string = '';
  gender: string = ''

  CLASSES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

  public ngOnInit(): void {

  }

  get f(): { [key: string]: AbstractControl } {
    return this.form.controls;
  }


  getDays(num: number, type: 'month' | 'year') {
    let month = this.form.controls['month'].value;
    let year = this.form.controls['year'].value;
    if (type === 'month') {
      month = num;
    } else if (type === 'year') {
      year = num;
    }
    this.DAYS = [];
    let currentDateStr = `${year}${String(month).padStart(2, '0')}01`;
    let _moment = moment(currentDateStr, "YYYYMMDD");
    let endMonthDay: number = _moment.daysInMonth();

    for (let i = 1; i <= endMonthDay; i++) {
      this.DAYS.push(i);
    }
    let day = this.form.get('day')?.value;
    if (this.DAYS.indexOf(day) < 0) {
      this.form.get('day')?.setValue(1);
    }

    return this.DAYS;
  }

  public ngOnDestroy(): void {
    this._destroySub$.next();
  }

  public onSubmit(): void {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    let user: User = new User();
    user.firstName = this.form.get('firstName')?.value;
    user.lastName = this.form.get('lastName')?.value;
    user.phone = this.form.get('phone')?.value;
    user.email = this.form.get('email')?.value;
    user.username = this.form.get('username')?.value;
    user.password = this.form.get('password')?.value;
    let year = this.form.get('year')?.value;
    let month = this.form.get('month')?.value;
    let day = this.form.get('day')?.value;
    user.dob = `${year}-${String(month).padStart(2, '0')}-${String(day).padStart(2, '0')}`;
    user.phone = this.form.get('phone')?.value;
    user.gender = this.form.get('gender')?.value;
    this._userService.register(user).subscribe(() => {
        localStorage.setItem('login_after_signup_name', user.phone)
        this._router.navigate(["/login"])
      },
      errorRes => {
        this.signupErrorMessage = errorRes.error.message
      })
  }
}
