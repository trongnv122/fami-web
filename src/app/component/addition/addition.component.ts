import {Component, ElementRef, HostListener, QueryList, ViewChildren} from '@angular/core';
import {AdditionQuestion} from "../../models/Unit";
import {ExamService} from "../../service/exam.service";
import {Exam} from "../../models/exam";
import {UserService} from "../../service/user.service";
import {User} from "../../models/user";


@Component({
  selector: 'app-login',
  templateUrl: './addition.component.html',
  styleUrls: ['./addition.component.scss'],
})
export class AdditionComponent {
  DATA: AdditionQuestion [] = []

  POINTS: number = 0;
  POINT_TOTAL: number = 60;
  TIME_TOTAL = 60;
  START_TIME = new Date().getTime();
  TAKE_TIME = 0;
  REMAINING_TIME = this.TIME_TOTAL;
  DONE: boolean = false;

  user: User = new User();

  constructor(private examService: ExamService,
              private userService:UserService) {
    while (this.DATA.length <= 60) {
      let q: AdditionQuestion = new AdditionQuestion();
      q.type = Math.floor(Math.random() * 2) === 1 ? 'C' : 'T';
      q.n1 = Math.floor(Math.random() * 10000000);
      if (q.type === 'T') {
        q.n2 = Math.floor(Math.random() * q.n1);
      } else {
        q.n2 = Math.floor(Math.random() * 10000000);
      }

      this.DATA.push(q);
    }


    let thiz = this;
    setInterval(() => {
      thiz.TAKE_TIME = Math.floor((new Date().getTime() - this.START_TIME) / 1000 / 60);
      this.REMAINING_TIME = this.TIME_TOTAL - thiz.TAKE_TIME;
      if (this.REMAINING_TIME <= 0) {
        thiz.DONE = true;
      }
    }, 60 * 1000)


    this.userService.userSubject.subscribe(user => {
      this.user = user || new User();
    })
  }

  change(item: AdditionQuestion) {
    if (item.type =='C' && item.n1 + item.n2 === item.answer) {
      item.result = 'T'
    }
    else if (item.type =='T' && item.n1 - item.n2 === item.answer) {
      item.result = 'T'
    } else {
      item.result = 'F'
    }

    this.POINTS = 0;
    this.DATA.map(i => {
      if (i.result === 'T') {
        this.POINTS++
      }
    })
  }
  @ViewChildren('el')element: QueryList<ElementRef> | null = null;
  position = 1;

  @HostListener('window:keyup', ['$event'])
  setSelection(event: any) {
    if (event && this.element) {
      for (let i=0; i < this.element.length; i++) {
        let target = this.element.get(i)?.nativeElement;
        target.type = 'text';
        target.setSelectionRange(0, 0);
        target.type = 'number';
      }
    }
  }

  submit () {
    let exam: Exam = new Exam();
    exam.name = 'Thực hiện phép tính cộng';
    exam.code = 'addition';
    exam.point = this.POINTS;
    exam.totalPoint = this.POINT_TOTAL;
    exam.totalTime = this.TIME_TOTAL;
    exam.takeTime = this.TAKE_TIME;
    this.examService.submit(exam).subscribe(result => {
      this.DONE = true;
    });
  }
}
