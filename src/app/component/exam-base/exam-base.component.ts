import {Component} from '@angular/core';
import {User} from "../../models/user";
import {ExamService} from "../../service/exam.service";
import {UserService} from "../../service/user.service";
import {Exam, Question} from "../../models/exam";
import * as moment from "moment/moment";


@Component({
  selector: 'exam-base',
  template: '<div></div>',
  styleUrls: ['./exam-base.component.scss'],
})
export abstract class ExamBaseComponent<T extends Question> {
  data: T[] = [];
  exam: Exam = new Exam();

  POINT_TOTAL: number = 60;
  TIME_TOTAL = 60;
  START_TIME = new Date().getTime();
  TAKE_TIME = 0;
  REMAINING_TIME = this.TIME_TOTAL;
  DONE: boolean = false;

  user: User = new User();

  abstract examName():string;
  abstract examCode():string;

  protected constructor(
    private examService: ExamService,
    private userService: UserService
  ) {
    this.data = this.initData();
    this.exam.startDate = moment(this.START_TIME).format('YYYY-MM-DD HH:mm:ss')

    setInterval(() => {
      this.TAKE_TIME = Math.floor((new Date().getTime() - this.START_TIME) / 1000 / 60);
      this.REMAINING_TIME = this.TIME_TOTAL - this.TAKE_TIME;
      if (this.REMAINING_TIME <= 0) {
        this.DONE = true;
      }
    }, 1000)


    this.userService.userSubject.subscribe(user => {
      this.user = user || new User();
    })
  }

  abstract initData(): T[];

  submitExam() {
    this.exam.name = this.examName();
    this.exam.code = this.examCode();
    this.exam.point = this.getPoint();
    this.exam.totalPoint = this.POINT_TOTAL;
    this.exam.totalTime = this.TIME_TOTAL;
    this.exam.takeTime = this.TAKE_TIME;
    this.exam.startDate = moment(this.START_TIME).format('YYYY-MM-DD HH:mm:ss')
    this.examService.submit(this.exam).subscribe(() => this.DONE = true);
  }

  getPoint() {
    return this.data.filter(q => q.isTrue()).length
  }

  goDown() {
    window.scroll({
      top: document.body.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });

  }

  goUp() {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });

  }

}
