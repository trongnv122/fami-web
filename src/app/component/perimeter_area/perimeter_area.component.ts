import {Component} from '@angular/core';
import {ExamService} from "../../service/exam.service";
import {UserService} from "../../service/user.service";
import {ExamBaseComponent} from "../exam-base/exam-base.component";
import {Question} from "../../models/exam";

interface Sharp {
  type: 'HCN' | 'HV',
  label: string
}

interface ExpressType {
  type: 'DT' | 'CV',
  label: string
}

const SHARPS: Sharp[] = [
  {type: 'HCN', label: 'hình chữ nhật'},
  {type: 'HV', label: 'hình vuông'},
]
const EXPRESS_TYPES: ExpressType[] = [
  {type: 'DT', label: 'diện tích'},
  {type: 'CV', label: 'chu vi'},
]

interface SharpQuestion extends Question {
  expressType: ExpressType;
  sharp: Sharp;
  d1: number,
  d2: number,
  group: string,
  option1: number,
  option2: number,
  option3: number,
  option4: number,
  answer_correct: number,
  answer: number,
}

@Component({
  selector: 'perimeter_area',
  templateUrl: './perimeter_area.component.html',
  styleUrls: ['./perimeter_area.component.scss'],
})
export class Perimeter_areaComponent extends ExamBaseComponent<SharpQuestion> {
  examName(): string {
      return 'Tìm chu vi và diện tích'
  }
  examCode(): string {
      return 'Sharp_Calculation'
  }
  initData(): SharpQuestion[] {

    let initData: (Question | any) [] = [];

    for (let i = 0; i < this.POINT_TOTAL; i++) {

      let question: SharpQuestion = {
        expressType: EXPRESS_TYPES[Math.floor(Math.random() * 2)],
        sharp: SHARPS[Math.floor(Math.random() * 2)],
        d1: 0,
        d2: 0,
        group: ("" + i),
        option1: 0,
        option2: 0,
        option3: 0,
        option4: 0,
        answer_correct: 0,
        answer: 0,
        isTrue() {
          return this.answer == this.answer_correct;
        },
        makeAnswer(option) {
          this.checked = true;
          this.answer = option;
        }
      }

      initData.push(question);
    }

    initData.map(q => {
      q.d1 = Math.floor(Math.random() * 100) + 1
      if (q.sharp.type == 'HCN') {
        q.d2 = Math.floor(Math.random() * 100 + 1)
      } else if (q.sharp.type == 'HV') {
        q.d2 = q.d1;
      }

      if (q.expressType.type == 'DT') {
        q.answer_correct = q.d1 * q.d2;
      } else if (q.expressType.type == 'CV') {
        q.answer_correct = (q.d1 + q.d2) * 2;
      }

      let S0 = q.answer_correct;
      let doubleAnswer = 2 * q.answer_correct;
      let S1 = Math.floor(Math.random() * doubleAnswer);
      let S2 = Math.floor(Math.random() * doubleAnswer);
      let S3 = Math.floor(Math.random() * doubleAnswer);

      let randomRangeAnswer = [S0, S1, S2, S3].sort(() => 0.5 - Math.random());

      q.option1 = randomRangeAnswer[0];
      q.option2 = randomRangeAnswer[1];
      q.option3 = randomRangeAnswer[2];
      q.option4 = randomRangeAnswer[3];
    })

    return initData;
  }

  constructor(
    examService: ExamService,
    userService: UserService
  ) {
    super(examService, userService);
    console.log("")
  }
}
