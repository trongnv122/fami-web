import {Component} from '@angular/core';
import {RANG_G, RANG_M, RANG_M2, UnitBase, UnitConvertQuestion} from "../../models/Unit";
import {ExamService} from "../../service/exam.service";
import {Exam} from "../../models/exam";
import {UserService} from "../../service/user.service";
import {User} from "../../models/user";
import * as moment from 'moment';

@Component({
  selector: 'app-login',
  templateUrl: './unit-conversation.component.html',
  styleUrls: ['./unit-conversation.component.scss'],
})
export class UnitConversationComponent {
  DATA: UnitConvertQuestion [] = []
  TYPES: any = {
    '0': RANG_M,
    '1': RANG_M2,
    '2': RANG_G,
  }

  POINTS: number = 0;
  POINT_TOTAL: number = 60;
  TIME_TOTAL = 60;
  START_TIME = new Date();
  TAKE_TIME = 0;
  REMAINING_TIME = this.TIME_TOTAL;
  DONE: boolean = false;

  user: User = new User();

  constructor(private examService: ExamService,
              private userService:UserService) {
    while (this.DATA.length <= 60) {
      let UNITS: UnitBase[] = this.TYPES[Math.floor(Math.random() * 3) + ''];

      let item: UnitConvertQuestion = new UnitConvertQuestion();
      item.l1_qty = Math.floor(Math.random() * 10)
      let u1Index= Math.max(1, Math.floor(Math.random() * (UNITS.length + 1)) - 1);
      item.l1_unit = UNITS[u1Index];

      if (item.l1_unit.exponent !== 1) {
        item.l2_qty = Math.floor(Math.random() * 10)
        item.l2_unit = UNITS[Math.max(0, Math.floor(Math.random() * u1Index - 1))];
      }

      if (item.l2_qty === 0 && item.l1_qty === 0) {
        continue;
      }

      item.answer_qty = Math.pow(10, item.l1_unit.exponent - item.l2_unit.exponent) * item.l1_qty + item.l2_qty;

      this.DATA.push(item);
    }


    let thiz = this;
    setInterval(() => {
      thiz.TAKE_TIME = Math.floor((new Date().getTime() - this.START_TIME.getTime()) / 1000 / 60);
      this.REMAINING_TIME = this.TIME_TOTAL - thiz.TAKE_TIME;
      if (this.REMAINING_TIME <= 0) {
        thiz.DONE = true;
      }
    }, 60 * 1000)


    this.userService.userSubject.subscribe(user => {
      this.user = user || new User();
    })
  }

  change(item: UnitConvertQuestion) {
    if (item.answer_qty === item.l3_qty) {
      item.result = 'T'
    } else {
      item.result = 'F'
    }

    this.POINTS = 0;
    this.DATA.map(i => {
      if (i.result === 'T') {
        this.POINTS++
      }
    })
  }

  submit () {
    let exam: Exam = new Exam();
    exam.name = 'Đổi đơn vị';
    exam.code = 'conversation';
    exam.point = this.POINTS;
    exam.totalPoint = this.POINT_TOTAL;
    exam.totalTime = this.TIME_TOTAL;
    exam.takeTime = this.TAKE_TIME;
    exam.startDate = moment(this.START_TIME).format('YYYY-MM-DD HH:mm:ss')
    this.examService.submit(exam).subscribe(result => {
      this.DONE = true;
    });
  }
}
