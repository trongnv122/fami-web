export class ListRequest {
  page: number = 0;
  size: number = 25;
  query: any = {};
  sort: Sort = new Sort();
}
export class Sort {
  columns: Column[] = [];
}

export class Column {
  name: string = '';
  direction: 'Ascending' | 'Descending' = 'Ascending';
}
