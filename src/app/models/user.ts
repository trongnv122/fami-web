export class User {
  id: string = '';
  username: string = '';
  password: string = '';
  firstName: string = '';
  lastName: string = '';
  email: string = '';
  sbd: string = '';
  phone: string = '';
  dob: string = '';
  gender: string = '';
}
