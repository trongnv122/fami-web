export class Exam {
  id: string = '';
  name: string = '';
  code: string = '';
  userId: string = '';
  point: number = 0;
  totalPoint: number = 0;
  totalTime: number = 0;
  takeTime:  number = 0;
  startDate: string = '';
}

export interface Question {
  makeAnswer(option: any): void;
  isTrue(): boolean;
  checked?: boolean;
}
