export class UnitBase {
  code: string = ''
  name: string = ''
  exponent: number = 1
}


export const RANG_M: UnitBase[] = [
  {code: 'mm', name: 'mi li mét', exponent: 1},
  {code: 'cm', name: 'cen ti-mét', exponent: 2},
  {code: 'dm', name: 'đề xi mét', exponent: 3},
  {code: 'm', name: 'mét', exponent: 4},
  {code: 'dam', name: 'đề ca mét', exponent: 5},
  {code: 'hm', name: 'héc tô mét', exponent: 6},
  {code: 'km', name: 'ki lô mét', exponent: 7}
]

export const RANG_M2: UnitBase[] = [
  {code: 'cm2', name: 'cen ti-mét vuông', exponent: 1},
  {code: 'dm2', name: 'đề xi mét vuông', exponent: 3},
  {code: 'm2', name: 'mét vuông', exponent: 5},
  {code: 'dam2', name: 'đề ca mét vuông', exponent: 7},
  {code: 'hm2', name: 'héc tô mét vuông', exponent: 9},
  {code: 'km2', name: 'ki lô mét vuông', exponent: 11}
]

export const RANG_G: UnitBase[] = [
  {code: 'g', name: 'gam', exponent: 1},
  {code: 'dag', name: 'đề ca gam', exponent: 2},
  {code: 'hg', name: 'héc tô gam', exponent: 3},
  {code: 'kg', name: 'ki lô gam', exponent: 4},
  {code: 'yến', name: 'yến', exponent: 5},
  {code: 'tạ', name: 'tạ', exponent: 6},
  {code: 'tấn', name: 'tấn', exponent: 7},
]


export class UnitConvertQuestion {
  l1_qty: number = 0;
  l1_unit: UnitBase = new UnitBase();

  l2_qty: number = 0;
  l2_unit: UnitBase = new UnitBase();

  l3_qty: number = 0;
  answer_qty: number = 0;
  result: string = '';
}


export class AdditionQuestion {
  type: string = 'C'; // C hoặc T
  n1: number = 0;
  n2: number = 0;
  answer: number = 0;
  result: string = '';
}
