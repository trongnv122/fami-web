##!/bin/bash

npm run build --prod
tar -cvf dist.tar dist
rsync -avzh dist.tar fami:/home/ubuntu/app/
ssh fami /home/ubuntu/app/deploy.sh
